#!/bin/bash

# La base pour programmer sainement avec bash ^^
set -euxo pipefail

# User do, quand on est en sudo, exécuter en tant que l'utilisateur qui a appelé
# le script.
user=${SUDO_USER-${USER}}
udo() {
    sudo -u $user "$@"
}

# Installer python3
DEBIAN_FRONTEND=noninteractive apt-get install -y python3-minimal

# Installer pip
eval PIP=~$user/.local/bin/pip3
if ! test -f ${PIP} ; then
    wget -O - https://bootstrap.pypa.io/get-pip.py | udo python3 - --user
fi

# Installer Ansible. À noter qu'Ansible déclenche un bug de Python 3.5 assez
# pénible. Résultat, je préfère réinstaller ansible avec python3.6 une fois que
# pyenv est configuré.
udo sh -c "${PIP} install --user --upgrade ansible"
