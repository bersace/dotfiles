;;; .doom.d/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here

(setq doom-theme 'doom-gruvbox)
(setq confirm-kill-emacs nil)
(use-package! openwith
  :config
  (setq openwith-associations (list (list ".+" "zed" '(file))))
  (openwith-mode 1))

(use-package! magit
  :config
  (transient-replace-suffix 'magit-push "p"
    '("p" magit-push-implicitly)))
