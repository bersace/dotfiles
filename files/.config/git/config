#
# Dans git, je référence toujours le projet amont dans origin. Je référence mon
# fork dans le remote bersace. Certains alias se basent là dessus.
#
# En général, je supprime la branche master locale. Si possible, je met le
# remote origin en HTTPS, pour désactiver le push non-interactif sur origin. Ça
# m'évite de pousser dans master par erreur, malgré l'invite de commande. S'il
# faut pousser sur master, Je pousse directement vers l'URL git plutôt que
# d'enregistrer le remote en écriture : git push
# git@gitlab.com:bersace/dotfiles.git. Le mieux, c'est quand c'est un script de
# livraison qui le fait.
#
# Je crée systématiquement mes branches à partir de origin/master et non depuis
# la branche locale, potentiellement obsolète ou pire, avec des commits locaux.
#

[include]
    path = config.local

[core]
    editor = zed --wait

[alias]
    # Liste les changements après git pull.
    changements = log --no-merges --reverse @{1}..@
    # Remonte dans le temps
    annuler = reset --hard @{1}

    # Retourne le nom de la branche amont.
    amont = rev-parse --abbrev-ref --symbolic-full-name @{upstream}
    # Retourne le commit depuis lequel la branche a divergé de la branche amont.
    fork-point = merge-base HEAD @{upstream}

    ### VUES
    # Historique concis
    h1 = log --cherry-pick --abbrev-commit --date=relative --pretty=format:'%C(cyan)%h%C(reset) %C(green)(%ad)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(yellow)%d%C(reset)'
    # Historique global
    graphe = h1 --all --graph
    # Liste les commits de la branche, façon PR GitHub.
    vpr = !r=$(git fork-point) && echo "Branche amont: $(git amont)" && echo && git diff --shortstat $r..HEAD  && git --no-pager diff --name-status $r.. && echo && git --no-pager h1 --reverse $r..
    # Liste les commits de la branche, façon MR GitLab.
    vmr = !r=$(git fork-point) && echo "Branche amont: $(git amont)" && echo && git --no-pager h1 $r..

    ### REBASAGE ###
    # Post-dater les commits de la PR.
    postdater = !git filter-branch -f --commit-filter 'sleep 1 && git commit-tree "$@"' --env-filter 'export GIT_AUTHOR_DATE=$(date -Is) GIT_COMMITTER_DATE=$(date -Is)' $(git fork-point)..HEAD

    ### NETTOYAGE
    # Nettoyer les branches locales mergées dans origin/master
    clean-branches = !git branch --merged @{u} | grep -Fv -e master -e '*' -e '+' | xargs -rl git branch -d && git remote prune origin
    # Sometimes, when you do lots of rebasing/squashing, your git directory becomes very heavy.
    # With this command, git automatically removes any orphan blob in the .git/ directory.
    # Tested and approved! For a very often rebased repo, turned it from 4.8GB to 350MB...
    mrpropre = -c gc.reflogExpire=0 -c gc.reflogExpireUnreachable=0 -c gc.rerereresolved=0 -c gc.rerereunresolved=0 -c gc.pruneExpire=now gc --aggressive

[branch]
    # Se souvenir de la branche d'origine (master par exemple).
    autoSetupMerge = true
    # Active pull --rebase sur les branches créé depuis origin/*
    autoSetupRebase = remote

[color]
    ui = auto

[commit]
    gpgSign = true

[fetch]
    prune = true

[init]
    defaultBranch = master

[push]
    # Par défaut, pousser dans une branche distante éponyme plutôt que du nom de
    # la branche amont.
    default = current

[merge]
    conflictStyle = diff3

[rebase]
    autostash = true

[remote]
    # Par défaut, pousser dans mon fork.
    pushDefault = bersace

[status]
    submoduleSummary = true

[gitlab]
    user = bersace
[github]
    user = bersace
[github "gitlab.com/api/v4"]
    user = bersace

[sendemail]
    confirm = auto
