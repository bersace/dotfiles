### PYTHON
if hash pyenv &>/dev/null ; then
	OLDPATH=$PATH
	eval "$(pyenv init - --no-rehash)"
	PATH=$OLDPATH
	unset OLDPATH
	export PYENV_VIRTUALENV_DISABLE_PROMPT=1
fi

# NODE / NVM
if [ -s "$NVM_DIR/nvm.sh" ] ; then
	# shellcheck source=/dev/null
	. "$NVM_DIR/nvm.sh"
fi

if [ -s "$NVM_DIR/bash_completion" ] ; then
	# shellcheck source=/dev/null
	. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
fi
