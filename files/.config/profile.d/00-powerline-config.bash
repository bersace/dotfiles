# Accepter des déclarations de variables non exportées, vu que le fichier va
# être sourcé.
# shellcheck disable=SC2034

# Sur les serveurs, forcer COLORTERM.
if [ -v SSH_CLIENT ] && ! [ -v COLORTERM ]; then
	export COLORTERM=truecolor
fi

POWERLINE_ICONS=nerd-fonts

case "${OLDTERM-$TERM}" in
linux) # console système, surtout les VM.
	POWERLINE_PALETTE=8bit
	if [ -f /usr/share/consolefonts/ter-powerline-v16n.psf.gz ]; then
		POWERLINE_ICONS=powerline
	else
		POWERLINE_ICONS=compat
		declare -A POWERLINE_ICONS_OVERRIDES
		POWERLINE_ICONS_OVERRIDES=(
			['sep-fin']='>'
		)
	fi
	;;
esac
