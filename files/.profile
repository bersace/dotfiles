# -*- shell-script -*-
#
# ~/.profile est exécuté par la commande de connexion : shell en mode login,
# gnome-session en mode X11, sudo -i, etc. Tout ce qu'on exporte ici est partagé
# à toutes les commandes lancées dans la session : éditeurs en mode graphique,
# terminaux, etc. Faut bien retenir ceci, ce fichier est sourcé une fois par
# session !
#
# En mode systemd, le fichier .profile est zappé en faveur de
# .config/environment.d/.
#
# .bashrc n'est pas sourcé automatiquement par les shell de connexion. .profile
# chaîne .bashrc explicitement à la fin, si le shell est interactif.

if [ -n "${BASH_SOURCE-0}" ] ; then
	PROFILE=$(readlink -e "${BASH_SOURCE[0]}")
	PROFILE_HOME="${PROFILE%/*}"
else
	PROFILE_HOME=$HOME
fi

# Configuration de la locale. On préfère fr, ou au moins C.UTF-8. Et surtout, on
# ne configure pas de locale absente. Ça évite les avertissement intempestifs de
# setlocale.
unset LC_ALL LANG
for locale in fr_FR.UTF-8 fr_FR.utf8 C.UTF-8 C.utf8 C ; do
	if locale -a | grep -Fiq ${locale} ; then
		export LC_ALL="${locale}"
		break
	fi
done
unset locale
export LANG="$LC_ALL"

# Dans la même veine que container=lxc.
if [ -f /.dockerenv ] ; then
	export container=docker
fi

# Chargement de ~/.config/environment.d/, comme si on était dans une session
# systemd. En effet, au bon vieux temps de X11, un script shell initialisait la
# session X11 et chargeait .profile. Avec systemd et Wayland, le shell graphique
# n'est plus lancé dans un shell de connexion et donc .profile n'est chargé que
# pour les connexions SSH par exemple.
#
# On unifie en déplaçant les variables d'env dans .config/environment.d et en
# les chargeant dans .profile comme systemd. Ainsi, SSH, systemd, lxc-attach,
# etc. tout le monde récupère les mêmes variables d'environnement d'un seul
# endroit. C'est dommage de multiplier les fichiers, mais c'est ainsi.
#
# Un avantage est qu'on a plus besoin d'un fichier spécifique comme
# .config/email pour remplir des variables d'environnement locales. Il suffit de
# pousser la variable directement dans environment.d/.
#
for d in $(ls -d "${PROFILE_HOME}" "${HOME}" | sort -u) ; do
	for f in "$d/.config/environment.d"/*.conf ; do
		set -o allexport
		# shellcheck source=/dev/null
		[ -r "$f" ] && . "$f"
		set +o allexport
	done
done
unset d f

# Sourcer .bashrc pour les shell bash interactifs si .profile n'est pas sourcé
# par .bashrc cf.
# https://unix.stackexchange.com/questions/228314/sudo-command-doesnt-source-root-bashrc#228441
if [ -n "${BASH_VERSION-}" ] && ! [ -n "${BASHRC_HOME-}" ] && [ -z "${-##*i*}" ] ; then
	. "${PROFILE_HOME}/.bashrc"
fi
