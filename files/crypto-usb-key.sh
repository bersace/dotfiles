#!/bin/sh

#set -x

# Part of passwordless cryptofs setup in Debian Etch.
# See: http://wejn.org/how-to-make-passwordless-cryptsetup.html
# Author: Wejn <wejn at box dot cz>
#
# Updated by Rodolfo Garcia (kix) <kix at kix dot com>
# For multiple partitions
# http://www.kix.es/
#
# Updated by TJ <linux@tjworld.net> 7 July 2008
# For automatic detection of USB devices,
# detection and examination of *all* partitions on the device (not just partition #1),
# automatic detection of partition type, refactored, commented, debugging code.
#
# Updated by Hendrik van Antwerpen <hendrik at van-antwerpen dot net> 3 Sept 2008
# For encrypted key device support, also added stty support for not
# showing your password in console mode.

# define counter-intuitive shell logic values (based on /bin/true & /bin/false)
# NB. use FALSE only to *set* something to false, but don't test for
# equality, because a program might return any non-zero on error
TRUE=0
FALSE=1

# set DEBUG=$TRUE to display debug messages, DEBUG=$FALSE to be quiet
DEBUG=$FALSE
# DEBUG=$TRUE

# is stty available? default false
STTY=$FALSE
STTYCMD=false
# check for stty executable
if [ -x /bin/stty ]; then
	STTY=$TRUE
	STTYCMD=/bin/stty
elif [ `(busybox stty >/dev/null 2>&1; echo $?)` -eq $TRUE ]; then
	STTY=$TRUE
	STTYCMD="busybox stty"
fi

# print message to stderr
# usage: msg <command> "message" [switch]
# switch : switch used for echo to stderr
# using the switch -n will allow echo to write multiple messages
# to the same line
msg ()
{
	if [ $# -gt 0 ]; then
		# handle multi-line messages
		echo $2 | while read LINE; do
			# use stderr for all messages
			echo $3 "$2" >&2
		done
	fi
}

dbg ()
{
	if [ $DEBUG -eq $TRUE ]; then
		msg "$@"
	fi
}

# read password from console
# usage: readpass "prompt"
readpass ()
{
	if [ $# -gt 0 ]; then
		[ $STTY -ne $TRUE ] && msg TEXT "WARNING stty not found, password will be visible"
		echo -n "$1" >&2
		$STTYCMD -echo
		read -r PASS </dev/console >/dev/null
		[ $STTY -eq $TRUE ] && echo >&2
		$STTYCMD echo
	fi
	echo -n "$PASS"
}

dbg STATUS "Executing crypto-usb-key.sh ..."

# flag tracking key-file availability
OPENED=$FALSE

# temporary mount path for USB key
MD=/tmp-usb-mount

if [ "x$1" = "x" -o "x$1" = "xnone" ]; then
	# default key-file on the USB disk
	KEYFILE=.key
else
	KEYFILE=$1
fi

# If the file already exists use it.
# This is useful where an encrypted volume contains keyfile(s) for later
# volumes and is now mounted and accessible
if [ -f $KEYFILE ]; then
	dbg TEXT "Found $KEYFILE"
	cat $KEYFILE
	OPENED=$TRUE
	DEV="existing mount"
	LABEL=""
else
	# Is the USB driver loaded?
	cat /proc/modules | busybox grep usb_storage >/dev/null 2>&1
	USBLOAD=0$?
	if [ $USBLOAD -gt 0 ]; then
		dbg TEXT "Loading driver 'usb_storage'"
		modprobe usb_storage >/dev/null 2>&1
	fi

	# give the system time to settle and open the USB devices
	sleep 1

	# Are there any SCSI block devices?
	#ls -d /sys/block/sd* >/dev/null 2>&1
	ls -d /sys/block/* >/dev/null 2>&1
	SBD=$?

	if [ $SBD -eq $TRUE ]; then
		mkdir -p $MD
		dbg TEXT "Trying to get key-file '$KEYFILE' ..."
		#for SFS in /sys/block/sd*/sd??; do
		for SFS in /sys/block/*/*[1-9]; do
			dbg TEXT "Examining $SFS" -n
			# is it a USB device?
			ls -l ${SFS}/../device | busybox grep 'usb' >/dev/null 2>&1
			USB=0$?
			dbg TEXT ", USB=$USB" -n
			# Is the device removable?
			REMOVABLE=0`cat ${SFS}/../removable`
			dbg TEXT ", REMOVABLE=$REMOVABLE" -n
			#if [ $USB -eq $TRUE -a $REMOVABLE -eq 1 -a -f $SFS/dev ]; then
			if [ -f $SFS/dev ]; then
				dbg TEXT ", *possible key device*" -n
				DEV=`busybox basename $SFS`
				# Check if key device itself is encrypted
				/sbin/cryptsetup isLuks /dev/${DEV} >/dev/null 2>&1
				ENCRYPTED=0$?
				# Open crypted partition and prepare for mount
				if [ $ENCRYPTED -eq $TRUE ]; then
				    continue
				fi
				dbg TEXT ", device $DEV" -n
				# Use vol_id to determine label
				LABEL="`/sbin/blkid -o value -s LABEL /dev/${DEV}`"
				dbg TEXT ", label $LABEL" -n
				# Use vol_id to determine fstype
				FSTYPE="`/sbin/blkid -o value -s TYPE /dev/${DEV}`"
				dbg TEXT ", fstype $FSTYPE" -n
				# Is the file-system driver loaded?
				cat /proc/modules | busybox grep $FSTYPE >/dev/null 2>&1
				FSLOAD=0$?
				if [ $FSLOAD -gt 0 ]; then
					dbg TEXT ", loading driver for $FSTYPE" -n
					# load the correct file-system driver
					modprobe $FSTYPE >/dev/null 2>&1
				fi
				dbg TEXT ", mounting /dev/$DEV on $MD" -n
				mount /dev/${DEV} $MD -t $FSTYPE -o ro >/dev/null 2>&1
				dbg TEXT ", (`mount | busybox grep $DEV`)" -n
				if [ -f $MD/$KEYFILE ]; then
					dbg TEXT ", found $MD/$KEYFILE" -n
					cat $MD/$KEYFILE
					OPENED=$TRUE
				else
					dbg TEXT ", NOT found $MD/$KEYFILE" -n
				fi
				dbg TEXT ", umount $MD" -n
				umount $MD >/dev/null 2>&1
				# Close encrypted key device
				if [ $ENCRYPTED -eq $TRUE -a 0$DECRYPTED -eq $TRUE ]; then
					dbg TEXT ", closing encrypted device" -n
					/sbin/cryptsetup luksClose bootkey >/dev/null 2>&1
				fi
				dbg TEXT ", done\n\n" -n
				if [ $OPENED -eq $TRUE ]; then
					break
				fi
			else
				dbg TEXT ", device `busybox basename $SFS` ignored" -n
			fi
			dbg CLEAR ""
		done
	fi
fi

if [ $OPENED -ne $TRUE ]; then
	msg TEXT "FAILED to find suitable USB key-file ..."
	readpass "Try to enter the LUKS password: "
else
	msg TEXT "Success loading key-file from $SFS ($LABEL)"
fi
