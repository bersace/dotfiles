# .bashrc est sourcé par chaque bash interactifs non-login. La première étape
# est de s'assurer qu'on a bien .profile de chargé.
#
# Normalement, .profile est chargé une seule fois, par la commande racine de la
# session : console, X, SSH, terminal login, sudo, etc. Ainsi tous les logiciels
# de la session héritent des variables de .profile (ex: tous les shell de tmux).
# Il y a des exceptions. Par exemple le flatpak GNOME Builder exécute un
# terminal qui n'hérite pas de .profile.


# Référencer le chemin complet du bashrc et de son dossier parent. Le but est de
# référencer les fichiers .bash de mon home lorsque je source ~bersace/.bashrc
# en root ou postgres par exemple. Ces variables doivent être définie avant de
# resourcer .profile, pour que .profile casse la boucle de source.
BASHRC=$(readlink -e "${BASH_SOURCE[0]}")
BASHRC_HOME=${BASHRC%/*}

# Sourcer .profile si le PROFILE_HOME n'est pas défini. (Cas GNOME Builder).
if [ -z "${PROFILE_HOME-}" ] ; then
	# shellcheck source=/dev/null
	. "${BASHRC_HOME}/.profile"
fi

# Arrêter ici pour les shell non-interactifs.
if [ -n "${-##*i*}" ] ; then
	return
fi

# Recharger /etc/profile.d et charger ~/.config/profile.d.
#
# Les scripts /etc/profile.d contiennent des conditions sur l'exécution dans un
# Bash, VTE, Byobu, etc. Il faut les réexécuter à chaque fois qu'on ouvre un
# shell.

mapfile -t dirs < <(readlink -e /etc "${BASHRC_HOME}/.config" ~/.config | sort -u)
for d in "${dirs[@]}" ; do
	for i in "$d/profile.d"/*.{,ba}sh; do
		if [ -r "$i" ]; then
			# shellcheck source=/dev/null
			. "$i"
		fi
	done
done
unset d i dirs

# Adaptation dynamique à la taille de la fenêtre. Le variables LINES et COLUMNS
# sont mise-à-jour.
shopt -s checkwinsize

### HISTORIQUE
shopt -s histappend
HISTCONTROL=ignorespace:erasedup:
HISTIGNORE='git* push* -f*:git* reset* --hard*'
HISTFILESIZE=10000
HISTSIZE=10000
if [ -d ~/.cache ] ; then
	HISTFILE=~/.cache/bash_history
	export LESSHISTFILE=~/.cache/lesshst
fi

### INVITE DE COMMANDE

# Enregistrer dans $__EC le code de sortie de la précédente commande.
PROMPT_COMMAND=''

if declare -f _mise_hook &>/dev/null ; then
	PROMPT_COMMAND+='; _mise_hook'
fi

# powerline.bash fournit __update_ps1 pour exécution dans PROMPT_COMMAND. S'il
# est importé via profile.d/, l'activer, sinon définir avant une invite par
# défaut, statique.
if declare -f __update_ps1 &>/dev/null ; then
	PROMPT_COMMAND+='; __update_ps1'
	PS0='\e[s\e[2F\e[2K\e[u\e[0m'
else
	PS1='\u@\h:\w\n\$ '
fi

# Envoyer les infos à VTE.
if declare -f __vte_prompt_command &>/dev/null ; then
	PROMPT_COMMAND+='; __vte_prompt_command'
fi

PROMPT_COMMAND="${PROMPT_COMMAND#; }"
