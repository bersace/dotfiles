# Mes fichier de configurations

Comme beaucoup, je sauvegarde et je partage ma config dans un projet Ansible
versionné avec git. Bienvenue dans mon grenier !

![Capture d'écran](./mon-écran.png)


## Points forts

- Invite de [commande Bash très
  rapide](https://gitlab.com/bersace/powerline.bash), colorée, décorée,
  copiable.
- Virtualisation: VM et conteneur LXC en utilisateurs (dans ~/.local).
- DNS: résolution des conteneurs en .docker et des VM en `.virt`.

Les fichiers de configuration sont dans [files/](files/), peut-être y
découvrirez-vous quelques astuces. Je déploie avec Ansible. Les playbooks sont
séparés, mais pas tous indépendants.

Chaque playbooks est documenté grâce à un commentaire en tête de fichier. `make
help` présente l'inventaire de tout les playbooks documentés, dans l'ordre
alphabêtique.


``` console
$ git clone --recursive https://github.com/bersace/dotfiles.git && cd dotfiles
$ sudo ./bootstrap.sh  # Installer pip et ansible dans ~/.local/
$ make help
$ ansible-playbook bash.yml
```

## Déployer sur une machine distante

Le dossier [inventaire.d/](inventaire.d) référence les machines pour
Ansible. Y ajouter un fichier avec ses machines supplémentaires :

    echo guest.virt ansible_connection=lxc >> inventaire.d/my-lxc

Par défaut, les playbooks sont limités à `localhost`. Il faut explicitement
indiquer `--limit` pour configurer une autre machine :

    ansible-playbook invité.yml --limit guest.virt


## Déployer pour une VM locale

Les playbooks intéressants pour une VM locale de dév :

``` console
ansible-playbook bootstrap.yml --limit guest.virt
ansible-playbook invité.yml --limit guest.virt
ansible-playbook bash.yml --limit guest.virt
```
