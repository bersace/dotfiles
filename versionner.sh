#!/bin/bash -eu
#
# Ce script fait l'inverse d'Ansible : il copie les fichiers locaux dans files/.
# Deux dossiers sont candidats : ~/ et ~/.local/bin/. Par flemme, ces deux
# dossiers sont fusionnés dans files/.
#
# Avec ce script, j'édite mon .bashrc directement dans mon ~/ et je lance
# ./versionner.sh quand je veux sauvegarder mon travail et le déployer ailleurs.
#

find files/ -type f | while read -r f; do
	base="${f##files/}"
	for dir in ~ ~/.local/bin ; do
		if [ -f $dir/$base ] && ! [ "$dir/$base" -ef "files/$base" ]; then
			cp -vu "$dir/$base" "files/$base"
		fi
	done
done

# Lister les fichiers modifiés.
git diff --name-only files/
